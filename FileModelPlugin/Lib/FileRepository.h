#pragma once
#include <QList>
#include "AbstractCrudRepository.h"
#include "File.h"
class FileRepository:public AbstractCrudRepository<File,QString>
{
public:
    FileRepository() =default;
private:
    QList<File> m_list;

    virtual void save(const File&) override;
    virtual File find(const QString& id) const override;
    virtual QList<File> findAll() override;
};
