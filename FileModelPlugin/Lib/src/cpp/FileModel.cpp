#include "FileModel.h"
#include "Utility.h"
#include "LoggerProvider.h"
FileModel::FileModel(QObject *parent) : QAbstractTableModel(parent)
{
     lInfo("New model is created");
}

void FileModel::addFile(const File &file)
{
    beginResetModel();
    m_fileList.push_back(file);
    endResetModel();
    lInfo(QString("Added new file %1 to model").arg(file.toString()));
}

void FileModel::removeFile(int position)
{
    beginResetModel();
    m_fileList.removeAt(position);
    endResetModel();
    lInfo("Removed a file from model");
}

void FileModel::replaceFile(int position, const File &file)
{
    beginResetModel();
    auto tmp = m_fileList[position];
    m_fileList.replace(position,file);
    endResetModel();
    lInfo(QString("Replaced file %0 with %1 in model").arg(tmp.toString(),file.toString()));
}

void FileModel::serialize(QFile &file)
{
    lInfo("Serializing to "+file.fileName()+"...");
    file.open(QFile::WriteOnly);
    QDataStream stream(&file);
    stream<<m_fileList;
    lInfo("Serialization done");

}

void FileModel::deserialize(QFile &file)
{
    lInfo("Deserializing from "+file.fileName()+"...");
    beginResetModel();
    file.open(QFile::ReadOnly);
    QDataStream stream(&file);
    stream>>m_fileList;
    endResetModel();
    lInfo("Deserialization done");
}

int FileModel::rowCount(const QModelIndex &parent) const
{
    return m_fileList.length();
}

QVariant FileModel::data(const QModelIndex &index, int role) const
{
    if(role==Qt::DisplayRole)
        switch (index.column()) {
        case 0:
            return m_fileList[index.row()].path();
        case 1:
            return m_fileList[index.row()].size();
        case 2:
            return m_fileList[index.row()].creationDate().toString();
        }
    else
        switch (role) {
        case PathRole:
            return m_fileList[index.row()].path();
        case SizeRole:
            return m_fileList[index.row()].size();
        case CreationDateRole:
            return m_fileList[index.row()].creationDate().toString();
        }
    return QVariant::Invalid;
}

int FileModel::columnCount(const QModelIndex &parent) const
{
    return 3;
}

QVariant FileModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    if(orientation==Qt::Horizontal){
        if(role==Qt::DisplayRole){
            return m_headerDataList[section];
        }
    }
    return QVariant::Invalid;
}


QHash<int, QByteArray> FileModel::roleNames() const
{
    return
    {
        {PathRole,"path"},
        {SizeRole,"size"},
        {CreationDateRole,"creationDate"}
    };
}
