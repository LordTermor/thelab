#include "File.h"
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>
File::File(const QString &path)
{
    load(path);

}

QString File::path() const
{
    return m_path;
}

void File::load(const QString &path)
{
    m_path = path;
    struct stat st;
    stat(m_path.toUtf8(),&st);
    m_fileSize = int(st.st_size/1000); //st_size is bytes but kilobytes are needed

    m_creationDate = QDateTime::fromTime_t(st.st_ctime);
}

int File::size() const
{
    return m_fileSize;
}

QDateTime File::creationDate() const
{
    return m_creationDate;
}

bool File::isValid(){
    return !m_path.isEmpty() && access( m_path.toUtf8(), F_OK ) != -1;
}
