#pragma once
#include <QString>
#include <QDateTime>
#include <QDataStream>
//!
//! \brief The File class
//!
//! Class that represents a record of particular file in the filesystem
//!
class File
{

public:
    //!
    //! \brief File
    //! \param path full path to the file in the filesystem
    //!
    //! Constructs the file from the given path of local filesystem
    //!
    File(const QString &path);

    //!
    //! \brief File
    //!
    //! Constructs the invalid empty file
    //!
    File() = default;

    //!
    //! \brief Returns the file's path in the filesystem
    //! \return full path to the file in the filesystem
    //!
    QString path() const;
    //!
    //! \brief Loads the file from the filesystem
    //! \param path full path to the file in the filesystem
    //!
    void load(const QString &path);

    //!
    //! \brief Returns the file's size
    //! \return file size
    //!
    int size() const;

    //!
    //! \brief Returns the file's creation date
    //! \return QDateTime contains the file creation date
    //!
    QDateTime creationDate() const;

    //!
    //! \brief Returns if file is valid
    //! \return true if file exists and false otherwise
    //!
    bool isValid();

    //!
    //! \brief Serializes file to stream
    //! \param Stream
    //! \param File to serialize
    //! \return Input stream with serialized data
    //!
    friend QDataStream& operator <<(QDataStream& stream, File& file)
    {
        stream<<file.m_path<<file.m_fileSize<<file.m_creationDate;
        return stream;
    }
    //!
    //! \brief Deserializes file from stream
    //! \param Stream
    //! \param File to deserialize
    //! \return Input stream after deserialization
    //!
    friend QDataStream& operator >>(QDataStream& stream, File& file)
    {
        stream>>file.m_path>>file.m_fileSize>>file.m_creationDate;
        return stream;
    }
    //!
    //! \brief Serializes file list to stream
    //! \param Stream
    //! \param File List to serialize
    //! \return Input stream with serialized data
    //!
    friend QDataStream& operator <<(QDataStream& stream, QList<File>& fileList)
    {
        for(auto &el:fileList){
            stream<<el;
        }
        return stream;
    }
    //!
    //! \brief Deserializes file list from stream
    //! \param Stream
    //! \param File list to deserialize
    //! \return Input stream after deserialization
    //!
    friend QDataStream& operator >>(QDataStream& stream, QList<File>& fileList)
    {
        fileList.clear();
        while (!stream.atEnd()) {
            File el;
            stream>>el;
            fileList.append(el);
        }
        return stream;
    }
    //!
    //! \brief Converts class object to string
    //! \return String that contains information about this file
    //!
    QString toString() const{
        return QString("File('%1', %2, %3)").arg(m_path).arg(m_fileSize).arg(m_creationDate.toString());
    }


private:
    QString m_path;
    int m_fileSize;
    QDateTime m_creationDate;
};
