#ifndef FILEMODEL_H
#define FILEMODEL_H
#include <QAbstractListModel>
#include <QList>
#include <QFile>
#include "File.h"

class FileModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit FileModel(QObject *parent = nullptr);

private:
    QList<File> m_fileList;
    QStringList m_headerDataList{"Путь","Размер","Дата создания"};
    enum roles{
        PathRole = Qt::UserRole+1,
        SizeRole,
        CreationDateRole
    };

public:
    void addFile(const File &);
    void removeFile(int position);
    void replaceFile(int position,const File&);

    void serialize(QFile& file);
    void deserialize(QFile& file);

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    int columnCount(const QModelIndex &parent) const;

    inline QList<File> fileList() const
    {
        return m_fileList;
    }
    inline void setFileList(const QList<File> &value)
    {
        m_fileList = value;
    }

    // QAbstractItemModel interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    // QAbstractItemModel interface
public:
    QHash<int, QByteArray> roleNames() const override;
};

#endif // FILEMODEL_H
