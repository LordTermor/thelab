#pragma once
#include <QObject>
#include "AbstractPluginView.h"
#include "AbstractPresenter.h"
#include <memory>
class FileModelPresenter:public QObject,public AbstractPresenter
{
    Q_OBJECT
    Q_INTERFACES(AbstractPresenter)
public:
    FileModelPresenter(std::shared_ptr<AbstractPluginView> view,QObject* parent = nullptr):
        QObject(parent),
        m_view(view)
    {
        connect(dynamic_cast<QObject*>(m_view.get()),SIGNAL(onAddRequested(const QString&)),this,SLOT(onAddButton(const QString&)));
        connect(dynamic_cast<QObject*>(m_view.get()),SIGNAL(onRemoveRequested(int)),this,SLOT(onRemoveButton(int)));
        connect(dynamic_cast<QObject*>(m_view.get()),SIGNAL(onSaveRequested(const QString&)),this,SLOT(onSaveButton(const QString&)));
        connect(dynamic_cast<QObject*>(m_view.get()),SIGNAL(onLoadRequested(const QString&)),this,SLOT(onLoadButton(const QString&)));
        connect(dynamic_cast<QObject*>(m_view.get()),SIGNAL(replaceRequested(int,const QString&)),this,SLOT(onReplaceRequested(int,const QString&)));
        m_view->setFileModel(&m_model);
    }
    ~FileModelPresenter() = default;
protected:
    std::shared_ptr<AbstractPluginView> m_view;
    FileModel m_model;
protected slots:
    void onAddButton(const QString&);
    void onRemoveButton(int);
    void onSaveButton(const QString&);
    void onLoadButton(const QString&);
    void onReplaceRequested(int pos,const QString&);
};
