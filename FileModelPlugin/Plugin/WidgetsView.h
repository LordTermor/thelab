#pragma once
#include <QWidget>
#include "AbstractPluginView.h"
#include "AbstractWidgetsView.h"
#include "FileModelPresenter.h"

namespace Ui{
    class View;
}

class WidgetsView:public QWidget,public AbstractWidgetsView,public AbstractPluginView,public std::enable_shared_from_this<WidgetsView>
{
    Q_OBJECT
    Q_INTERFACES(AbstractPluginView)
    Q_INTERFACES(AbstractWidgetsView)

public:
    WidgetsView(QWidget* parent = nullptr);
    ~WidgetsView() override = default;

    std::shared_ptr<AbstractPresenter> bindPresenter() override{
        return std::make_shared<FileModelPresenter>(shared_from_this());
    }

protected:
    Ui::View* ui;
    std::shared_ptr<QMenu> m_menu;

    // AbstractPluginView interface
public:
    void setFileModel(FileModel *model) override;

signals:
    void onAddRequested(const QString&) override;
    void onRemoveRequested(int) override;
    void onLoadRequested(const QString&) override;
    void onSaveRequested(const QString&) override;
    void replaceRequested(int position, const QString&) override;

    // AbstractView interface
public:
    std::shared_ptr<QMenu> getMenu() override;
};
