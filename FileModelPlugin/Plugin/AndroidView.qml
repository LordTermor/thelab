import QtQuick 2.0
import QtQuick.Controls 2.12
import Lab.FileModel 1.0
import Qt.labs.platform 1.0
Page {
    anchors.fill: parent
    Column{
        anchors.fill: parent
        Row{
            ToolButton{
                id:addButton
                text: "➕"
                onClicked: {
                    fileDialog.open();
                }
            }
            ToolButton{
                id:removeButton
                text:"🗑"
            }

            ToolButton{
                id:saveButton
                text: "💾↑"
            }
            ToolButton{
                id:loadButton
                text: "💾↓"
            }
        }

        ListView{
            id:fileLsitView
            objectName: "fileListView"
            delegate: ItemDelegate{
                text: path
                height: 25
                width: parent.width
            }
        }
        FileDialog{
            id:fileDialog
            currentFile: document.source
            folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
            onAccepted: {
            }
        }
    }

}
