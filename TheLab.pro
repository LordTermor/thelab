TEMPLATE = subdirs

SUBDIRS = TheLabMain \
          FileModelPlugin \
          LowLevelPlugin \
          AnalyzerPlugin \
          LibLL \
          TheLabMainLib\
          ORMPlugin

FileModelPlugin.depends = TheLabMainLib
LowLevelPlugin.depends = TheLabMainLib
#LowLevelPlugin.depends = LibLL
AnalyzerPlugin.depends = TheLabMainLib
TheLabMain.depends = TheLabMainLib
