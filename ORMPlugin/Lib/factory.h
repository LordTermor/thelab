#pragma once
#include <string>
#include <odb/core.hxx>

#pragma db object
class Factory
{
public:
    int getFactoryid() const
    {
        return factoryid;
    }
    void setFactoryid(int value)
    {
        factoryid = value;
    }

    std::string getName() const
    {
        return name;
    }
    void setName(const std::string &value)
    {
        name = value;
    }

private:
    friend class odb::access;
#pragma db id
    int factoryid;
    std::string name;
};








