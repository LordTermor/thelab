#pragma once
#include <odb/core.hxx>
#include <string>

#pragma db object
class Lemonade
{
public:
    std::string getName() const
    {
        return name;
    }
    void setName(const std::string &value)
    {
        name = value;
    }

    int getVolume() const
    {
        return volume;
    }
    void setVolume(int value)
    {
        volume = value;
    }

    int getFactoryId() const
    {
        return factoryId;
    }
    void setFactoryId(int value)
    {
        factoryId = value;
    }

private:
    friend class odb::access;
    Lemonade() =default;

#pragma db id
    std::string name;
    int volume;
    int factoryId;
};
