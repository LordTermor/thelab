#-------------------------------------------------
#
# Project created by QtCreator 2019-03-23T19:26:32
#
#-------------------------------------------------

TARGET = ORM
TEMPLATE = lib
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    LemonadeTableModel.cpp

HEADERS += \
    Lemonade.h \
    Factory.h \
    LemonadeTableModel.h


unix {
    target.path = /usr/lib
    INSTALLS += target
}
DEFINES += PLUGINID=\\\"FileModelPlugin\\\"


DESTDIR = ../../TheLabMain/Libs

CONFIG += unversioned_libname unversioned_soname

INCLUDEPATH+=../../TheLabMainLib/include src/headers /usr/local/include/
LIBS+= -L/usr/local/lib
#-L../../TheLabMain -lTheLabMainLib


# List of header files that should be compiled with the ODB compiler.
#
ODB_FILES += Lemonade.h\
            Factory.h

# ODB compiler flags.
#
ODB_FLAGS = --database pgsql --generate-schema --generate-query --generate-session

# Select the database we are going to use.
#
DEFINES += DATABASE_PGSQL

# Suppress unknown pragmas GCC warnings.
#
QMAKE_CXXFLAGS_WARN_ON = $$QMAKE_CXXFLAGS_WARN_ON -Wno-unknown-pragmas

# Link to the ODB runtime libraries.
#
LIBS += -lodb-pgsql
LIBS += -lodb

# ODB compilation rules. Normally you don't need to change anything here.
#

# Add the Qt headers directory to the ODB include directory list.
#
ODB_FLAGS += -I$$[QT_INSTALL_HEADERS]

# Newer versions of QtCreator do builds in a separate directory. As a
# result, we need to append the source directory to ODB files.
#
for(dir, ODB_FILES) {
  ODB_PWD_FILES += $$PWD/$${dir}
}

odb.name = odb ${QMAKE_FILE_IN}
odb.input = ODB_PWD_FILES
odb.output = ${QMAKE_FILE_BASE}-odb.cxx
odb.commands = odb $$ODB_FLAGS ${QMAKE_FILE_IN}
odb.depends = $$ODB_PWD_FILES
odb.variable_out = SOURCES
odb.clean = ${QMAKE_FILE_BASE}-odb.cxx ${QMAKE_FILE_BASE}-odb.hxx ${QMAKE_FILE_BASE}-odb.ixx ${QMAKE_FILE_BASE}.sql
QMAKE_EXTRA_COMPILERS += odb

odbh.name = odb ${QMAKE_FILE_IN}
odbh.input = ODB_PWD_FILES
odbh.output = ${QMAKE_FILE_BASE}-odb.hxx
odbh.commands = @true
odbh.CONFIG = no_link
odbh.depends = ${QMAKE_FILE_BASE}-odb.cxx
QMAKE_EXTRA_COMPILERS += odbh
