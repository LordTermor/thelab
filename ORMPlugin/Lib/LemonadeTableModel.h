#pragma once
#include <odb/database.hxx>
#include <odb/transaction.hxx>
#include <vector>
#include <QAbstractTableModel>
#include "Lemonade-odb.hxx"
#include "Lemonade.h"

class LemonadeTableModel:public QAbstractTableModel
{
    Q_OBJECT
public:
     LemonadeTableModel() = default;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
private:
    odb::result<Lemonade> m_result;
};
