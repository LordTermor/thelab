#include "LemonadeTableModel.h"
#include "range/v3/all.hpp"
int LemonadeTableModel::rowCount(const QModelIndex &parent) const
{
    return m_result.size();
}

int LemonadeTableModel::columnCount(const QModelIndex &parent) const
{
    return 3;
}

QVariant LemonadeTableModel::data(const QModelIndex &index, int role) const
{
    if(role==Qt::DisplayRole){
        int i =0;
        auto result =  m_result;
        auto it =result.begin();
        for(;it!=result.end() && i<index.row();++it);
        switch (index.column()) {
        case 0:
            return QString::fromStdString((*it).getName());
        case 1:
            return (*it).getVolume();
        case 2:
            return (*it).getFactoryId();
        }
    }
    return QVariant::Invalid;
}
