#-------------------------------------------------
#
# Project created by QtCreator 2019-03-23T19:26:32
#
#-------------------------------------------------

TARGET = FileModelPlugin
TEMPLATE = lib
QT+=gui widgets quick
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    WidgetsView.cpp \
    AndroidView.cpp \
    FileModelPresenter.cpp \
    PluginManager.cpp

HEADERS += \
    WidgetsView.h \
    WidgetsViewFactory.h \
    AndroidView.h \
    AndroidViewFactory.h \
    AbstractPluginView.h \
    FileModelPresenter.h \
    PluginManager.h

INCLUDEPATH+=$$PWD/../TheLabMainLib
LIBS+=-L../../TheLabMain -lTheLabMainLib -L../../TheLabMain/Libs/ -lFileModel

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    plugin.json

FORMS += \
    View.ui

RESOURCES += \
    resources.qrc

DEFINES += PLUGINID=\\\"FileModelPlugin\\\"


DESTDIR = ../../TheLabMain/Plugins

CONFIG += unversioned_libname unversioned_soname
INCLUDEPATH+=../../TheLabMainLib/include ../Lib/src/headers
