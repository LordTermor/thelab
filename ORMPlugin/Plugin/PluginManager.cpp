#include "PluginManager.h"
#include "FileModelPresenter.h"
#include <AndroidViewFactory.h>
#include <WidgetsViewFactory.h>

PluginManager::PluginManager():
    AbstractPluginManager (PluginMetaInfo(PLUGINID,"Записи о файлах",1,0)),
    m_qmlFactory(std::make_shared<AndroidViewFactory>()),
    m_widgetsFactory(std::make_shared<WidgetsViewFactory>())
{

}

std::shared_ptr<AbstractViewFactory> PluginManager::getFactory(const QString &viewType) const
{
    if(viewType=="QML"){
        return m_qmlFactory;
    } else if(viewType=="Widgets"){
        return m_widgetsFactory;
    }
    return nullptr;
}
static const QStringList list {
    "QML",
    "Widgets"
};
QStringList PluginManager::getViewTypes() const
{
    return list;
}
