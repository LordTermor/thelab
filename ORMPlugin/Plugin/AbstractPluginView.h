#pragma once
#include <QtCore>
#include "FileModel.h"
class AbstractPluginView
{
public:
    virtual ~AbstractPluginView()=default;
    virtual void setFileModel(FileModel* model) = 0;

signals:
    virtual void onAddRequested(const QString&) = 0;
    virtual void onRemoveRequested(int) = 0;
    virtual void onLoadRequested(const QString&) = 0;
    virtual void onSaveRequested(const QString&) = 0;
    virtual void replaceRequested(int pos,const QString&) = 0;


};
Q_DECLARE_INTERFACE(AbstractPluginView,"AbstractPluginView")
