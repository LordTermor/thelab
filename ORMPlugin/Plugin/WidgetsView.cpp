#include "WidgetsView.h"
#include "ui_View.h"
#include <QFileDialog>
WidgetsView::WidgetsView(QWidget *parent):
    QWidget (parent),
    AbstractWidgetsView(),
    AbstractPluginView(),
    ui(new Ui::View)
{
    ui->setupUi(this);

    auto addLambda = [=](){
        QString path;
        if(!(path=QFileDialog::getOpenFileName(this,"Выберите файл")).isEmpty()){
            emit this->onAddRequested(path);
        }
    };
    auto removeLambda = [=](){
        emit this->onRemoveRequested(ui->fileView->currentIndex().row());
    };
    auto loadLambda = [=](){
        QString path;
        if(!(path=QFileDialog::getOpenFileName(this,"Выберите файл для загрузки")).isEmpty()){
            emit this->onLoadRequested(path);
        }
    };
    auto saveLambda = [=](){
        QString path;
        if(!(path=QFileDialog::getSaveFileName(this,"Выберите файл для сохранения")).isEmpty()){
            emit this->onSaveRequested(path);
        }
    };
    connect(ui->addToolButton,&QToolButton::clicked,this,addLambda);
    connect(ui->removeToolButton,&QToolButton::clicked,this,removeLambda);
    connect(ui->loadToolButton,&QToolButton::clicked,this,loadLambda);
    connect(ui->saveToolButton,&QToolButton::clicked,this,saveLambda);
    connect(ui->fileView,&QTableView::doubleClicked,this,[=](const QModelIndex& index){
        QString path;
        if(!(path=QFileDialog::getOpenFileName(this,"Выберите файл")).isEmpty()){
            emit this->replaceRequested(index.row(),path);
        }
    });

    m_menu = std::make_shared<QMenu>(this);

    QAction* addAction = new QAction("Добавить файл",this);
    connect(addAction,&QAction::triggered,this,addLambda);
    QAction* removeAction = new QAction("Удалить файл",this);
    connect(removeAction,&QAction::triggered,this,removeLambda);
    QAction* loadAction = new QAction("Загрузить список",this);
    connect(loadAction,&QAction::triggered,this,loadLambda);
    QAction* saveAction = new QAction("Сохранить список",this);
    connect(saveAction,&QAction::triggered,this,saveLambda);

    m_menu->addActions({addAction,removeAction,loadAction,saveAction});

    ui->fileView->verticalHeader()->hide();
    ui->fileView->setSelectionBehavior(QTableView::SelectRows);
}

void WidgetsView::setFileModel(FileModel *model)
{
    ui->fileView->setModel(model);
    ui->fileView->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
    ui->fileView->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    ui->fileView->horizontalHeader()->setSectionResizeMode(2,QHeaderView::ResizeToContents);

}

std::shared_ptr<QMenu> WidgetsView::getMenu()
{
    return m_menu;
}
