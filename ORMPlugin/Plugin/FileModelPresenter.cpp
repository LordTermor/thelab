#include "FileModelPresenter.h"

void FileModelPresenter::onAddButton(const QString &path)
{
    m_model.addFile(File(path));
}

void FileModelPresenter::onRemoveButton(int index)
{
    m_model.removeFile(index);
}

void FileModelPresenter::onSaveButton(const QString &path)
{
    QFile file(path);
    m_model.serialize(file);
}

void FileModelPresenter::onLoadButton(const QString &path)
{
    QFile file(path);
    m_model.deserialize(file);
}

void FileModelPresenter::onReplaceRequested(int pos, const QString &path)
{
    File file(path);
    m_model.replaceFile(pos,file);
}
