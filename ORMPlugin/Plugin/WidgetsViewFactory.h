#pragma once
#include "AbstractViewFactory.h"
#include "WidgetsView.h"

class WidgetsViewFactory:public AbstractWidgetsViewFactory
{
public:
    virtual ~WidgetsViewFactory() = default;
    // ViewFactory interface
public:
    std::shared_ptr<AbstractWidgetsView> create(QWidget* parent = nullptr) override{return std::make_shared<WidgetsView>(parent);}
};
