#include "LibLL.h"
#include <iostream>

int LibLL::binaryNot(int lh)
{
    int res;
#ifdef __amd64__
    asm("mov %1, %%eax\n\t"
        "not %%eax\n\t"
        "mov %%eax, %0"
        :"=r"(res)
        :"r"(lh)
        :"eax"
        );
#endif
#ifdef __arm__

#endif
    return res;
}
