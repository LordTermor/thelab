#ifndef SYSTEMPROGLIBLL_H
#define SYSTEMPROGLIBLL_H

#include "sysproglibll_global.h"
#include <QObject>

class SYSPROGLIBLLSHARED_EXPORT LibLL : public QObject
{
Q_OBJECT
        Q_PLUGIN_METADATA(IID "SystemProgLibLL" FILE "llplugin.json")
public:
    explicit LibLL(QObject* parent = nullptr):QObject(parent){

    }
    Q_INVOKABLE static int binaryNot(int lh);
};
#endif // SYSTEMPROGLIBLL_H
