#include <utility>

#include "SettingsBackendQSettings.h"


SettingsBackendQSettings::SettingsBackendQSettings(QString path, QSettings::Format format):m_settings(path,format),m_folder(std::move(path))
{

}

void SettingsBackendQSettings::save(const QString &category, const QString &key, const QVariant &value)
{
    m_settings.setValue(key,value);
}

QVariant SettingsBackendQSettings::load(const QString &category, const QString &key)
{
    return m_settings.value(key,QVariant::Invalid);

}
