#include "SettingsProvider.h"
#include "Utility.h"
#include "Settings.h"
static auto& settings = Utility::Singleton<Settings>::instance();
SettingsProvider::SettingsProvider(const QString &pluginId):m_pluginId(pluginId)
{}

QVariant SettingsProvider::load(const QString &key)
{
    return settings.load(m_pluginId,key);
}

void SettingsProvider::save(const QVariantMap &map)
{
    settings.save(m_pluginId,map);
}

void SettingsProvider::save(const QString &key, const QVariant &value)
{
    settings.save(m_pluginId,key,value);
}
