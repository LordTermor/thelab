#include "Logger.h"
static QString pattern = "[%0] [%1] %2";
Logger::Logger(QObject *parent) : QObject(parent)
{

}

void Logger::logWarning(const QString &str)
{
    pLogWarning("MAIN",str);
}

void Logger::logCritical(const QString &str)
{
    pLogCritical("MAIN",str);
}

void Logger::logInfo(const QString &str)
{
    pLogInfo("MAIN",str);
}

void Logger::pLogWarning(const QString& category,const QString &str)
{
    m_backend->writeWarning(pattern.arg("WARNING",category,str));
}

void Logger::pLogCritical(const QString& category,const QString &str)
{
    m_backend->writeCritical(pattern.arg("CRITICAL",category,str));
}

void Logger::pLogInfo(const QString& category,const QString &str)
{
     m_backend->writeInfo(pattern.arg("INFO",category,str));
}
