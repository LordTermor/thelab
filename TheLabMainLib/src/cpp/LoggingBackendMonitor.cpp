#include "LoggingBackendMonitor.h"

LoggingBackendMonitor::LoggingBackendMonitor()
{

}

QList<std::shared_ptr<AbstractLoggingBackend> > LoggingBackendMonitor::backendList() const
{
    return m_backendList;
}

void LoggingBackendMonitor::setBackendList(const QList<std::shared_ptr<AbstractLoggingBackend> > &backendList)
{
    m_backendList = backendList;
}

void LoggingBackendMonitor::addBackend(const std::shared_ptr<AbstractLoggingBackend> &backend)
{
    m_backendList.append(backend);
}


void LoggingBackendMonitor::writeInfo(const QString &message)
{
    for(auto& el:m_backendList){
        el->writeInfo(message);
    }
}

void LoggingBackendMonitor::writeWarning(const QString &message)
{
    for(auto& el:m_backendList){
        el->writeWarning(message);
    }
}

void LoggingBackendMonitor::writeCritical(const QString &message)
{
    for(auto& el:m_backendList){
        el->writeCritical(message);
    }
}
