#include "LoggingBackendRetranslator.h"

LoggingBackendRetranslator::LoggingBackendRetranslator(QObject *parent):QObject (parent)
{

}


void LoggingBackendRetranslator::writeInfo(const QString &message)
{
    emit infoWritten(message);
}

void LoggingBackendRetranslator::writeWarning(const QString &message)
{
    emit warningWritten(message);
}

void LoggingBackendRetranslator::writeCritical(const QString &message)
{
    emit criticalWritten(message);
}
