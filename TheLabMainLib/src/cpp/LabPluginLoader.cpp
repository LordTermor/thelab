#include "LabPluginLoader.h"
#include "Utility.h"
#include "Logger.h"
#include <QDir>
using namespace  Utility;
LabPluginLoader::LabPluginLoader(const QString &pluginLocation, QObject *parent):
    QObject (parent),
    m_pluginLocation(pluginLocation)
{

}

void LabPluginLoader::load(){
    QDir dir(m_pluginLocation);
#ifdef __linux__
    dir.setNameFilters({"*.so"});
#endif
#ifdef __APPLE__
    dir.setNameFilters({"*.dylib"});
#endif
#ifdef _WIN64
    dir.setNameFilters({"*.dll"});
#endif
    for(auto& el:dir.entryList()){
        m_pluginLoader.setFileName(m_pluginLocation+"/"+el);
        if(m_pluginLoader.load()){
            if(qobject_cast<AbstractPluginManager*>(m_pluginLoader.instance())!=nullptr){
                m_pluginsList.append(qobject_cast<AbstractPluginManager*>(m_pluginLoader.instance()));
                Singleton<Logger>::instance().logInfo(QString("Library %1 loaded. Id: %2").arg(el).arg(m_pluginsList.last()->metaInfo().id));
            }
            else
                Singleton<Logger>::instance().logWarning(QString("Library %1 is not The Lab plugin").arg(el));

        } else
            Singleton<Logger>::instance().logCritical(QString("Library %1 cannot be loaded! Reason is %2")
                                                      .arg(el)
                                                      .arg(m_pluginLoader.errorString()));
    }
}


