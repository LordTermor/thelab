#include "Settings.h"

Settings::Settings(QObject *parent) : QObject(parent)
{

}

QVariant Settings::load(const QString &key)
{
    return m_backend->load("MAIN",key);
}

void Settings::save(const QVariantMap &map)
{
    for (auto it = map.keyValueBegin();it!=map.keyValueEnd();++it) {
        m_backend->save("MAIN",(*it).first,(*it).second);
    }
}

void Settings::save(const QString &key, const QVariant &value)
{
    m_backend->save("MAIN",key,value);
}

void Settings::setBackend(const std::shared_ptr<AbstractSettingsBackend> &backend)
{
    m_backend = backend;
}

QVariant Settings::load(const QString &category, const QString &key)
{
    return m_backend->load(category,key);
}

void Settings::save(const QString &category, const QVariantMap &map)
{
    for (auto it = map.keyValueBegin();it!=map.keyValueEnd();++it) {
        m_backend->save(category,(*it).first,(*it).second);
    }
}

void Settings::save(const QString &category, const QString &key, const QVariant &value)
{
    m_backend->save(category,key,value);
}
