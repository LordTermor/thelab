#include <utility>

#include "LoggerProvider.h"
#include "Logger.h"
#include "Utility.h"
static auto &logger = Utility::Singleton<Logger>::instance();
LoggerProvider::LoggerProvider(QString m_id):m_pluginId(std::move(m_id))
{
    
}

void LoggerProvider::logInfo(const QString &msg)
{
    logger.pLogInfo(m_pluginId,msg);
}

void LoggerProvider::logCritical(const QString &msg)
{
    logger.pLogCritical(m_pluginId,msg);
}

void LoggerProvider::logWarning(const QString &msg)
{
    logger.pLogWarning(m_pluginId,msg);
}
