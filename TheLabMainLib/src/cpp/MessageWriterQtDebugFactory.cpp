#include "MessageWriterQtDebugFactory.h"
#include "LoggingBackendQtDebug.h"

MessageWriterQtDebugFactory::MessageWriterQtDebugFactory()
{

}

std::shared_ptr<AbstractLoggingBackend> MessageWriterQtDebugFactory::create()
{
    return std::dynamic_pointer_cast<AbstractLoggingBackend>(std::make_shared<MessageWriterQtDebugFactory>());
}
