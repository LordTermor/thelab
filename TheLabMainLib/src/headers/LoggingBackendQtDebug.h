#pragma once
#include <QObject>
#include <QtDebug>
#include "AbstractLoggingBackend.h"

class LoggingBackendQtDebug:public AbstractLoggingBackend
{
public:

    // AbstractLogger interface
public:
    void writeInfo(const QString& message) override{
        qInfo()<<message;
    }
    void writeWarning(const QString& message) override{
        qWarning()<<message;
    }
    void writeCritical(const QString& message) override{
        qCritical()<<message;
    }
};
