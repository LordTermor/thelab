#pragma once
#include <QString>
class AbstractLoggingBackend
{
public:
    AbstractLoggingBackend() =default;
    virtual ~AbstractLoggingBackend() = default;

    virtual void writeInfo(const QString& message) = 0;
    virtual void writeWarning(const QString& message) = 0;
    virtual void writeCritical(const QString& message) = 0;
};
