#include "AbstractLoggingBackend.h"
#include <QList>
class LoggingBackendMonitor:public AbstractLoggingBackend
{
public:
    LoggingBackendMonitor();

    QList<std::shared_ptr<AbstractLoggingBackend> > backendList() const;
    void setBackendList(const QList<std::shared_ptr<AbstractLoggingBackend> > &backendList);
    void addBackend(const std::shared_ptr<AbstractLoggingBackend>& backend);
private:
    QList<std::shared_ptr<AbstractLoggingBackend>> m_backendList;

    // AbstractMessageWriter interface
public:
    void writeInfo(const QString &message) override;
    void writeWarning(const QString &message) override;
    void writeCritical(const QString &message) override;

};
