#ifndef LABPLUGINLOADER_H
#define LABPLUGINLOADER_H

#include <QObject>
#include <QString>
#include <QPluginLoader>
#include "AbstractPluginManager.h"

class LabPluginLoader:public QObject
{
    Q_OBJECT
public:
    LabPluginLoader(const QString& pluginLocation,QObject* parent = nullptr);

    void load();

    inline QList<AbstractPluginManager *> pluginsList() const
    {
        return m_pluginsList;
    }

private:
    QList<AbstractPluginManager*> m_pluginsList;
    QString m_pluginLocation;
    QPluginLoader m_pluginLoader;
};

#endif // LABPLUGINLOADER_H
