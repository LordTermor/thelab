#pragma once

#include <QObject>
#include "AbstractLoggingBackend.h"

class Logger : public QObject
{
    Q_OBJECT
    friend class LoggerProvider;
public:
    explicit Logger(QObject *parent = nullptr);
    void logWarning(const QString& str);
    void logCritical(const QString& str);
    void logInfo(const QString& str);
    void setLogger(std::shared_ptr<AbstractLoggingBackend> backend){
        m_backend=backend;
    }
private:
    void pLogWarning(const QString& category,const QString& str);
    void pLogCritical(const QString& category,const QString& str);
    void pLogInfo(const QString& category,const QString& str);

    std::shared_ptr<AbstractLoggingBackend> m_backend;
};
