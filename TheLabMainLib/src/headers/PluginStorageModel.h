#ifndef PLUGINSTORAGEMODEL_H
#define PLUGINSTORAGEMODEL_H

#include <QObject>
#include <QList>
#include "AbstractPresenter.h"
#include "AbstractWidgetsView.h"

class PluginStorageModel:public QObject
{
public:
    PluginStorageModel(QObject* parent = nullptr);
    inline void appendPresenter(const QString& id,std::shared_ptr<AbstractPresenter> presenter){
        m_presenters.insert(id,presenter);
    }
private:
    QMap<QString,std::shared_ptr<AbstractPresenter>> m_presenters;
};

#endif // PLUGINSTORAGEMODEL_H
