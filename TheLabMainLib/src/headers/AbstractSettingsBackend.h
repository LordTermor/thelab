#pragma once
#include <QVariant>
class AbstractSettingsBackend
{
public:
    virtual ~AbstractSettingsBackend() = default;
    virtual void save(const QString& category, const QString& value, const QVariant&)  =0;
    virtual QVariant load(const QString& category, const QString&) = 0;
};
