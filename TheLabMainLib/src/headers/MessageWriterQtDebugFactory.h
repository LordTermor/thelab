#pragma once
#include <memory>
#include "AbstractMessageWriterFactory.h"
class MessageWriterQtDebugFactory:public AbstractMessageWriterFactory
{
public:
    MessageWriterQtDebugFactory();
    std::shared_ptr<AbstractLoggingBackend> create() override;
};

