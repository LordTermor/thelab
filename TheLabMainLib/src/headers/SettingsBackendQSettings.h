#pragma once
#include "AbstractSettingsBackend.h"
#include <QSettings>
class SettingsBackendQSettings:public AbstractSettingsBackend
{
public:
    SettingsBackendQSettings(QString  path,QSettings::Format);
private:
    QSettings m_settings;
QString m_folder;
QSettings::Format m_format;
    // AbstractSettingsBackend interface
public:
    void save(const QString& category, const QString &key, const QVariant &value) override;
    QVariant load(const QString& category, const QString &key) override;
};
