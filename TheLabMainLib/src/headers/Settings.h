#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include "AbstractSettingsBackend.h"
#include <memory>
class Settings : public QObject
{
    Q_OBJECT
    friend class SettingsProvider;
public:
    explicit Settings(QObject *parent = nullptr);
    QVariant load(const QString& key);
    void save(const QVariantMap& map);
    void save(const QString& key,const QVariant& value);
    void setBackend(const std::shared_ptr<AbstractSettingsBackend> &backend);

private:
    std::shared_ptr<AbstractSettingsBackend> m_backend;
    //MEOW!
    QVariant load(const QString& category,const QString& key);
    void save(const QString& category,const QVariantMap& map);
    void save(const QString& category,const QString& key,const QVariant& value);
};

#endif // SETTINGS_H
