#pragma once
#include <QObject>
#include "AbstractLoggingBackend.h"
class LoggingBackendRetranslator:public QObject, public AbstractLoggingBackend
{
    Q_OBJECT
public:
    LoggingBackendRetranslator(QObject *parent = nullptr);

    // AbstractMessageWriter interface
public:
    void writeInfo(const QString &message) override;
    void writeWarning(const QString &message) override;
    void writeCritical(const QString &message) override;
signals:
    void infoWritten(const QString &message);
    void warningWritten(const QString& message);
    void criticalWritten(const QString& message);
};
