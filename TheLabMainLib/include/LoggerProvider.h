#pragma once
#include <QString>
class LoggerProvider
{
public:
    explicit LoggerProvider(QString  id);

    void logInfo(const QString&);
    void logCritical(const QString&);
    void logWarning(const QString&);
private:
    QString m_pluginId;
};

#define lWarning(msg) LoggerProvider(PLUGINID).logWarning(msg)
#define lCritical(msg) LoggerProvider(PLUGINID).logCritical(msg)
#define lInfo(msg) LoggerProvider(PLUGINID).logInfo(msg)
