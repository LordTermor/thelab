#pragma once
#include "AbstractViewFactory.h"
#include "AbstractQmlView.h"
#include <QQmlEngine>
#include <memory>
class AbstractQmlViewFactory:public AbstractViewFactory
{
public:
    virtual std::shared_ptr<AbstractQmlView> create(QQmlEngine* engine = nullptr) = 0;
    virtual ~AbstractQmlViewFactory() =default;
};
