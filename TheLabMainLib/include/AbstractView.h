#pragma once
#include <QtCore>
#include <QMenu>
#include "AbstractPresenter.h"
class AbstractView
{
public:
    virtual ~AbstractView() = default;
    virtual std::shared_ptr<AbstractPresenter> bindPresenter() = 0;
    virtual std::shared_ptr<QMenu> getMenu() = 0;
};

Q_DECLARE_INTERFACE(AbstractView,"AbstractView")
