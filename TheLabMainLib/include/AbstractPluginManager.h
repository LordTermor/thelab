#include <utility>

#pragma once
#include "AbstractQmlViewFactory.h"
#include "AbstractWidgetsViewFactory.h"
#include "AbstractPresenter.h"
#include <QtDebug>
#include "LoggerProvider.h"
struct PluginMetaInfo{
    PluginMetaInfo(
            QString  id,
            QString  name,
            int majorVer,
            int minorVer):
        name(std::move(name)),
        id(std::move(id)),
        majorVer(majorVer),
        minorVer(minorVer)
    {}
    QString name;
    QString id;
    int majorVer;
    int minorVer;
};
class AbstractPluginManager:public QObject
{
public:
    AbstractPluginManager(PluginMetaInfo info):m_info(std::move(info)){
    }
    AbstractPluginManager(AbstractPluginManager const&) = delete;
    AbstractPluginManager& operator= (AbstractPluginManager const&) = delete;
    virtual ~AbstractPluginManager()=default;

    virtual std::shared_ptr<AbstractViewFactory> getFactory(const QString& viewType) const  = 0;
    virtual QStringList getViewTypes() const = 0;

    PluginMetaInfo metaInfo(){
        return m_info;
    }

private:
    PluginMetaInfo m_info;
};

Q_DECLARE_INTERFACE(AbstractPluginManager,"AbstractPluginManager")
