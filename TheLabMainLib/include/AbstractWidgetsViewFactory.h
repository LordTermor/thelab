#pragma once
#include "AbstractWidgetsView.h"
#include <QWidget>
#include "AbstractViewFactory.h"

class AbstractWidgetsViewFactory:public AbstractViewFactory
{
public:
    virtual std::shared_ptr<AbstractWidgetsView> create(QWidget* parent = nullptr) = 0;
    virtual ~AbstractWidgetsViewFactory() = default;
};
