#pragma once
#include <QtCore>
#include <AbstractView.h>
class AbstractQmlView:public AbstractView
{
public:
    virtual ~AbstractQmlView()=default;
};

Q_DECLARE_INTERFACE(AbstractQmlView,"AbstractQmlView")
