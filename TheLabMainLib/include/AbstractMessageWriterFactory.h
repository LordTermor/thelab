#pragma once
#include <AbstractLoggingBackend.h>
#include <memory>
class AbstractMessageWriterFactory
{
public:
    AbstractMessageWriterFactory() = default;
    virtual ~AbstractMessageWriterFactory() = default;
    virtual std::shared_ptr<AbstractLoggingBackend> create() = 0;
};
