#pragma once
#include <QtCore>
#include <AbstractView.h>
class AbstractWidgetsView:public AbstractView
{
public:
    virtual ~AbstractWidgetsView()=default;
};
Q_DECLARE_INTERFACE(AbstractWidgetsView,"AbstractWidgetsView")
