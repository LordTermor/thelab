#pragma once
#include <QtCore>

class AbstractPresenter
{
public:
    virtual ~AbstractPresenter() = default;
};
Q_DECLARE_INTERFACE(AbstractPresenter,"AbstractPresenter")
