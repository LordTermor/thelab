#ifndef SETTINGSPROVIDER_H
#define SETTINGSPROVIDER_H
#include <QVariant>

class __attribute__((visibility("default"))) SettingsProvider
{
public:
    SettingsProvider(const QString& pluginId);

    QVariant load(const QString& key);
    void save(const QVariantMap& map);
    void save(const QString& key,const QVariant& value);
private:
    QString m_pluginId;
};

#endif // SETTINGSPROVIDER_H
