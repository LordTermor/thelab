#pragma once
namespace Utility {

template<typename T>
class  Singleton{
public:

    Singleton() =delete;
    Singleton( const Singleton& ) = delete;
    Singleton& operator=( Singleton& ) = delete;
    static T& instance(){
        static T singleton;
        return singleton;
    }
};
}
