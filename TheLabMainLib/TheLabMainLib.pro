#-------------------------------------------------
#
# Project created by QtCreator 2019-03-07T01:59:13
#
#-------------------------------------------------

QT += core gui qml quickcontrols2

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DEFINES+=WIDGETSVIEW

TEMPLATE = lib

TARGET = TheLabMainLib
CONFIG-=app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


CONFIG += unversioned_libname unversioned_soname
DESTDIR = ../TheLabMain/


INCLUDEPATH+=src/headers/\
            include/ \
            /usr/local/


HEADERS += \
    include/AbstractMessageWriterFactory.h \
    include/AbstractPluginManager.h \
    include/AbstractPresenter.h \
    include/AbstractQmlView.h \
    include/AbstractQmlViewFactory.h \
    include/AbstractView.h \
    include/AbstractViewFactory.h \
    include/AbstractWidgetsView.h \
    include/AbstractWidgetsViewFactory.h \
    include/SettingsProvider.h \
    include/Utility.h \
    include/LoggerProvider.h \
    src/headers/AbstractLoggingBackend.h \
    src/headers/AbstractSettingsBackend.h \
    src/headers/LabPluginLoader.h \
    src/headers/Logger.h \
    src/headers/LoggingBackendMonitor.h \
    src/headers/LoggingBackendQtDebug.h \
    src/headers/LoggingBackendRetranslator.h \
    src/headers/MessageWriterQtDebugFactory.h \
    src/headers/PluginStorageModel.h \
    src/headers/Settings.h \
    src/headers/SettingsBackendQSettings.h \
    include/Exceptions/TheLabException.h \
    include/Exceptions/PluginCannotBeLoadedException.h \
    AbstractCrudRepository.h

SOURCES += \
    src/cpp/LabPluginLoader.cpp \
    src/cpp/Logger.cpp \
    src/cpp/LoggerProvider.cpp \
    src/cpp/LoggingBackendMonitor.cpp \
    src/cpp/LoggingBackendRetranslator.cpp \
    src/cpp/MessageWriterQtDebugFactory.cpp \
    src/cpp/PluginStorageModel.cpp \
    src/cpp/Settings.cpp \
    src/cpp/SettingsBackendQSettings.cpp \
    src/cpp/SettingsProvider.cpp \
    src/cpp/Utility.cpp
