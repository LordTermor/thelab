#include "Analyzer.h"
#include "LoggerProvider.h"

static QMap<QString,std::function<int(int,int)>> operationTable{
{"++",[](int value,int){return value+1;}},{"--",[](int value,int){return value-1;}},
{"+=",[](int v1,int v2){return v1+v2;}},{"-=",[](int v1,int v2){return v1-v2;}},
{"*=",[](int v1,int v2){return v1*v2;}},{"/=",[](int v1,int v2){return v1/v2;}}
                                                 };
static QMap<QString,std::function<bool(int,int)>> conditionTable{
{"<=",[](int v1,int v2){return v1<=v2;}},{">=",[](int v1,int v2){return v1>=v2;}},
{"==",[](int v1,int v2){return v1==v2;}},{"!=",[](int v1,int v2){return v1!=v2;}},
{"<",[](int v1,int v2){return v1<v2;}},{">",[](int v1,int v2){return v1>v2;}},
{"&&",[](int v1,int v2){return v1&&v2;}},{"||",[](int v1,int v2){return v1||v2;}}
                                                  };

const QRegularExpression Analyzer::doWhileRegexp =
        QRegularExpression("\\A((int\\s*[a-zA-Z]+\\s*=\\s*\\d+;\\s*)+)do\\s*{\\s*(((([a-zA-Z]+[\\+\\-]{2})|[a-zA-Z]+[\\*\\-\\/\\+]\\=\\w+);\\s*)+)}\\s*while\\s*\\((([a-zA-Z]*(>|<|==|<=|>=|!=)\\w+)(\\s*(&&|\\|\\|)\\s*([a-zA-Z]*(>|<|==|<=|>=|!=)\\w+))*)\\s*\\)\\s*;\\s*\\z");

Analyzer::Analyzer(QObject *parent) : QObject(parent)
{

}

bool Analyzer::parse(const QString &input,int* number)
{

    auto match = doWhileRegexp.match(input);
if(match.hasMatch()){
    auto list = match.capturedTexts();

    //Needed results are in 1, 3 and 7 groups
    m_processor.setMap(processInitializationBlock(list[1]));
    m_processor.setList(processBodyBlock(list[3]));
    m_processor.setCondition(processConditionBlock(list[7]));

    *number = m_processor.process();
    return true;
}
    return false;
}

QMap<QString, int> Analyzer::processInitializationBlock(const QString &input,bool* ok)
{
    QMap<QString,int> result;

    auto list = input.split(QRegularExpression(";\\s*"),QString::SkipEmptyParts);

    static QRegularExpression initRegexp("\\Aint\\s*([a-zA-Z]+)\\s*=\\s*(\\d+)\\z");

    for(auto& el:list){
        auto match = initRegexp.match(el);
        auto captures = match.capturedTexts();
        if(!result.contains(captures[1]))
            result.insert(captures[1],captures[2].toInt());
        else {
            if(ok!=nullptr)
                *ok=false;
            return result;
        }

    }
    if(ok!=nullptr)
        *ok=true;
    return result;
}

InstructionList Analyzer::processBodyBlock(const QString &input,bool* ok )
{
    InstructionList result;

    auto list = input.split(QRegularExpression(";\\s*"),QString::SkipEmptyParts);

    QRegularExpression bodyRegexp("((\\w+)([\\+\\-]{2}))|((\\w+)([\\*\\-\\/\\+]\\=)(\\w+))");

    for(auto& el:list){
        auto match = bodyRegexp.match(el);
        auto captures = match.capturedTexts();
        QString varResName;
        QString var1Name;
        QString var2Name;

        QString operationString;
        if(captures[1]!=""){  // i++/i--
            varResName = captures[2];
            var1Name=varResName;
            operationString = captures[3];
            var2Name="";
        } else{ // i x= n
            varResName = captures[5];
            var1Name=varResName;

            var2Name = captures[7];
            operationString = captures[6];
        }
        arithmeticInstruction instr;
        instr.v1=var1Name;
        instr.v2=var2Name;
        instr.vres=varResName;
        instr.operation=operationTable[operationString];
        result.append(instr);


    }

    return result;
}

ConditionList Analyzer::processConditionBlock(const QString &input, bool *ok)
{
    ConditionList result;

    //    for(auto & el:list){

    auto regexp = QRegularExpression("(\\w+)\\s*(>|<|==|<=|>=|!=)\\s*(\\w+)\\s*(\\|\\||&&|)\\s*");
    auto matchIterator = regexp.globalMatch(input);
    int i =1;
    QString betweenOperationVarName("@l1");
    QString lastBetweenOperation;

    auto match = matchIterator.next();
    auto captures = match.capturedTexts();

    logicalInstruction instr;
    instr.vres = QString("@l%0").arg(i);
    instr.v1 = captures[1];
    instr.v2 = captures[3];
    instr.operation=conditionTable[captures[2]];
    result.append(instr);

    if(captures[4]!=""){
        lastBetweenOperation = captures[4];
    }

    while(matchIterator.hasNext()){
        i++;
        auto match = matchIterator.next();
        auto captures = match.capturedTexts();

        logicalInstruction instr;
        instr.vres = QString("@l%0").arg(i);
        instr.v1 = captures[1];
        instr.v2 = captures[3];
        instr.operation=conditionTable[captures[2]];
        result.append(instr);

        logicalInstruction instr1;
        instr1.vres = betweenOperationVarName+QString("_%0").arg(i);
        betweenOperationVarName = instr1.vres;
        instr1.v1 = QString("@l%0").arg(i-1);
        instr1.v2 = QString("@l%0").arg(i);

        instr1.operation=conditionTable[lastBetweenOperation];
        if(captures[4]!=""){
            lastBetweenOperation=captures[4];
        }

        result.append(instr1);
    }



    return result;
}

int TLILProcessor::process()
{
    bool condResult = true;
    int counter = 0;
    QString dumpString;
    for(auto it = m_map.keyValueBegin();it!=m_map.keyValueEnd();++it){
        dumpString+=QString("%0 = %1\n").arg((*it).first,(*it).second);
    }
    lInfo(QString("Translation started, initial dump is:\n%0").arg(dumpString));
    while(condResult && counter<15000){

        counter++;
        for(auto& instr:m_list){
            process(instr);
        }
        for(auto& instr:m_condition){
             process(instr);
             condResult = m_map[instr.vres];
        }
        QString dumpString;
        for(auto it = m_map.keyValueBegin();it!=m_map.keyValueEnd();++it){
            dumpString+=QString("%0 = %1\n").arg((*it).first,QString::number((*it).second));
        }
        lInfo(QString("Iteration: %0, the dump is:\n%1").arg(counter).arg(dumpString));
    }
    lInfo(QString("Translation ended, number of iterations:%0").arg(counter));
    return counter;

}

VariablesMap TLILProcessor::map() const
{
    return m_map;
}

void TLILProcessor::setMap(const VariablesMap &map)
{
    m_map = map;
}

ConditionList TLILProcessor::condition() const
{
    return m_condition;
}

void TLILProcessor::setCondition(const ConditionList &condition)
{
    m_condition = condition;
}

InstructionList TLILProcessor::list() const
{
    return m_list;
}

void TLILProcessor::setList(const InstructionList &list)
{
    m_list = list;
}
