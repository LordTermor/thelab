#pragma once

#include <QObject>
#include <QRegularExpression>
#include <QLinkedList>
using VariablesMap = QMap<QString,int>;
template <class T>
struct instruction{
    QString vres;
    QString v1;
    QString v2;

    std::function<T(int,int)> operation;
};
using arithmeticInstruction = instruction<int>;
using logicalInstruction = instruction<bool>;

using InstructionList = QLinkedList<arithmeticInstruction>;
using ConditionList = QLinkedList<logicalInstruction>;

class TLILProcessor:public QObject{
    Q_OBJECT
public:


    VariablesMap map() const;
    void setMap(const VariablesMap &map);

    ConditionList condition() const;
    void setCondition(const ConditionList &condition);

    InstructionList list() const;
    void setList(const InstructionList &list);

    int process();
private:


    VariablesMap m_map;
    ConditionList m_condition;
    InstructionList m_list;

    template<class T>
    T process(instruction<T>& inst){
        bool ok;
        int vInt1 = inst.v1.toInt(&ok);
        if(!ok){
            if(m_map.contains(inst.v1))
                vInt1=m_map[inst.v1];
        }
        int vInt2 = inst.v2.toInt(&ok);
        if(!ok){
            if(m_map.contains(inst.v2))
                vInt2=m_map[inst.v2];
        }
        m_map[inst.vres]=inst.operation(vInt1,vInt2);
        return m_map[inst.vres];
    }
};

class Analyzer : public QObject
{
    Q_OBJECT
public:
    explicit Analyzer(QObject *parent = nullptr);



public slots:
    bool parse(const QString&,int* number);

private:
    TLILProcessor m_processor;
    static const QRegularExpression doWhileRegexp;


    static QMap<QString,int> processInitializationBlock(const QString&, bool *ok=nullptr);
    static InstructionList processBodyBlock(const QString &input, bool *ok = nullptr);
    static ConditionList processConditionBlock(const QString &input, bool *ok = nullptr);
};
