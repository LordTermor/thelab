#pragma once
#include <QObject>
#include "AbstractPluginView.h"
#include "AbstractPresenter.h"
#include "Analyzer.h"
#include <memory>
class Presenter:public QObject,public AbstractPresenter
{
    Q_OBJECT
    Q_INTERFACES(AbstractPresenter)
public:
    Presenter(std::shared_ptr<AbstractPluginView> view,QObject* parent = nullptr):
        QObject(parent),
        m_view(view),
        m_analyzer(std::make_shared<Analyzer>())
    {
        connect(dynamic_cast<QObject*>(m_view.get()),SIGNAL(operationRequested()),this,SLOT(onAnalyzeRequested()));
    }
    ~Presenter() = default;
public slots:
    void onAnalyzeRequested();
protected:
    std::shared_ptr<AbstractPluginView> m_view;
    std::shared_ptr<Analyzer> m_analyzer;
};
