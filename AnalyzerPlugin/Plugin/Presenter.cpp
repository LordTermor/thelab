#include "Presenter.h"

void Presenter::onAnalyzeRequested()
{
    int number;
    bool result = m_analyzer->parse(m_view->getInputString(),&number);

    if(result){
        m_view->setResult(number);
        m_view->setError(false);
    } else {
        m_view->setError(true);
    }
}
