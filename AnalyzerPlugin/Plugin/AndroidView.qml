import QtQuick 2.0
import QtQuick.Controls 2.12
import Lab.FileModel 1.0
Page {
    anchors.fill: parent
    Column{
        anchors.fill: parent
        Row{
            ToolButton{
                id:addButton
                text: "➕"
            }
            ToolButton{
                id:removeButton
                text:"🗑"
            }

            ToolButton{
                id:saveButton
                text: "💾↑"
            }
            ToolButton{
                id:loadButton
                text: "💾↓"
            }
        }

        ListView{

            model:FileModel{

            }

            delegate: ItemDelegate{
                height: 25
                width: parent.width
            }
        }
    }

}
