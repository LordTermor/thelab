#pragma once
#include <QWidget>
#include "AbstractPluginView.h"
#include "AbstractWidgetsView.h"
#include "Presenter.h"

namespace Ui{
    class View;
}

class WidgetsView:
        public QWidget,
        public AbstractWidgetsView,
        public AbstractPluginView,
        public std::enable_shared_from_this<WidgetsView>
{
    Q_OBJECT
    Q_INTERFACES(AbstractPluginView)
    Q_INTERFACES(AbstractWidgetsView)

public:
    WidgetsView(QWidget* parent = nullptr);
    virtual ~WidgetsView() override = default;



public:
    std::shared_ptr<AbstractPresenter> bindPresenter() override{
        return std::make_shared<Presenter>(shared_from_this());
    }

protected:
    Ui::View* ui;
    std::shared_ptr<QMenu> m_menu;


    // AbstractPluginView interface
public:
    void setResult(int) override;
    QString getInputString() override;
    void setError(bool) override;

signals:
    void operationRequested() override;

    // AbstractView interface
public:
    std::shared_ptr<QMenu> getMenu() override;
};
