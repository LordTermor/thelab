#pragma once
#include <QtCore>
class AbstractPluginView
{
public:
    virtual ~AbstractPluginView()=default;

    virtual void setResult(int) = 0;
    virtual QString getInputString() = 0;
    virtual void setError(bool) = 0;

signals:
    virtual void operationRequested() = 0;
};
Q_DECLARE_INTERFACE(AbstractPluginView,"AbstractPluginView")
