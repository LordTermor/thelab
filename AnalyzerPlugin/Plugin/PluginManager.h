#pragma once
#include "AbstractPluginManager.h"
#include "AbstractQmlViewFactory.h"
#include "AbstractWidgetsViewFactory.h"
#include "Presenter.h"
class PluginManager : public AbstractPluginManager
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID PLUGINID FILE "plugin.json")
    Q_INTERFACES(AbstractPluginManager)
public:
    PluginManager();

    // AbstractPluginManager interface
    std::shared_ptr<AbstractViewFactory> getFactory(const QString &viewType) const;
    QStringList getViewTypes() const;
private:
    std::shared_ptr<AbstractQmlViewFactory> m_qmlFactory;
    std::shared_ptr<AbstractWidgetsViewFactory> m_widgetsFactory;

};
