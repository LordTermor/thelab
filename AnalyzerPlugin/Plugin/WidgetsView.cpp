#include "WidgetsView.h"
#include "ui_View.h"
#include <QFileDialog>
WidgetsView::WidgetsView(QWidget *parent):
    QWidget (parent),
    AbstractWidgetsView(),
    AbstractPluginView(),
    ui(new Ui::View),
    m_menu(std::make_shared<QMenu>(this))
{
    ui->setupUi(this);

    connect(ui->checkButton,&QPushButton::clicked,this,&WidgetsView::operationRequested,Qt::DirectConnection);
}

void WidgetsView::setResult(int param)
{
    ui->outputLabel->setText(QString::number(param));
}

QString WidgetsView::getInputString()
{
    return ui->textBrowser->toPlainText();
}

void WidgetsView::setError(bool error)
{
    if(error){
        ui->label->setText("ОШИБКА ПРОВЕРКИ");
        ui->outputLabel->setText("");
    } else {
        ui->label->setText("Количество итераций:");
    }
}


std::shared_ptr<QMenu> WidgetsView::getMenu()
{
    return m_menu;
}
