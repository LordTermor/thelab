QT += core gui widgets qml quickcontrols2
TEMPLATE=app
CONFIG += c++17
CONFIG-=app_bundle

DEFINES+= QMLVIEW
# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH+=$$PWD/../TheLabMainLib/include\
                 $$PWD/../TheLabMainLib/src/headers

LIBS+=-L./ -lTheLabMainLib

RESOURCES += \
    src/qml/resources.qrc

DISTFILES += \
    src/qml/qmldir \
    src/qml/QmlDependencies.qml \
    src/qml/QmlMainWindowView.qml

HEADERS += \
    src/headers/MainWindowPresenter.h \
    src/headers/MainWindowView.h \
    src/headers/QmlMainWindowView.h\
    src/headers/AbstractMainWindowView.h

SOURCES += \
    src/cpp/main.cpp \
    src/cpp/MainWindowPresenter.cpp \
    src/cpp/MainWindowView.cpp \
    src/cpp/QmlMainWindowView.cpp

INCLUDEPATH+=$$PWD/src/headers


FORMS += \
    ui/MainWindowView.ui
