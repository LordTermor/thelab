import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.8
import QtQuick.Controls.Material 2.12
import QtQml.Models 2.12
ApplicationWindow {
    id:main
    objectName: "mainWindow"
    height: 500
    width: 300
    property var list: []
    property int currentIndex: -1
    header: ToolBar {
        id:toolBar
        Material.background: Material.BlueGrey
        RowLayout {
            ToolButton {
                onClicked:  drawer.open();
                Column{
                    anchors.centerIn: parent
                    spacing: 3
                    Rectangle {
                        id: bar1
                        width: 19
                        height: 2
                    }

                    Rectangle {
                        id: bar2
                        width:19
                        height:2
                    }

                    Rectangle {
                        id: bar3
                        width: 19
                        height: 2
                    }
                }
            }
            anchors.fill: parent

            Label {
                font.pixelSize: 17
                id: titleLabel
                font.family: "Roboto"
                text: model.get(currentIndex).modelData
            }
        }
    }

    Drawer {
        id: drawer
        width: 0.66 * parent.width
        height: parent.height
        ListView{
            anchors.fill: parent
            header:Rectangle{
                color: Qt.darker(toolBar.Material.background,1.1)
                height: 125
                width: parent.width
                Label{
                    anchors{
                        margins: 5
                        left:parent.left
                        bottom: parent.bottom
                    }
                    Material.foreground: "white"
                    font.family: "Roboto"
                    text: "Лабораторная работа"
                }
            }

            model: ListModel{
                id:model
            }

            delegate:  ItemDelegate{
                highlighted:index==currentIndex
                width: parent.width
                text: modelData
                onClicked: {
                    currentIndex=index
                    stackView.replace(list[index])
                    drawer.close();
                }
            }
        }

    }


    Pane{
        anchors.fill: parent
        StackView{
            anchors.fill: parent
            id:stackView
            objectName: "tabView"

            replaceEnter: Transition {
                NumberAnimation{
                    property:"opacity"
                    from:0
                    to:1
                    easing.type: Easing.InQuad
                }
            }
            replaceExit: Transition {
                NumberAnimation{
                    property:"opacity"
                    from:1
                    to:0
                    easing.type: Easing.OutQuad
                }
            }
        }
    }
    function append(name,item1){
        model.append({"modelData":name});
        list.push(item1);
    }
}
