#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include "AbstractMainWindowView.h"
#include "AbstractWidgetsView.h"
#include "AbstractPresenter.h"

namespace Ui {
class MainWindowView;
}

class MainWindowView : public QMainWindow,public AbstractMainWindowView
{
    Q_OBJECT
    Q_INTERFACES(AbstractMainWindowView)
public:
    explicit MainWindowView(QWidget *parent = nullptr);
    ~MainWindowView() override;
    std::shared_ptr<AbstractPresenter> bindView(AbstractPluginManager& tab) override;

private:
    QMap<QString,std::shared_ptr<AbstractWidgetsView>> m_viewsMap;
    Ui::MainWindowView *ui;
    QMainWindow* m_dockWidget;

    // AbstractMainWindowView interface
public:
    inline void show() override{QMainWindow::show();}
protected:
    void showEvent(QShowEvent* event) override;
    void resizeEvent(QResizeEvent* event) override;

    // AbstractMainWindowView interface
public:
    void appendTextToBrowser(const QString &) override;
};

#endif // MAINWINDOW_H
