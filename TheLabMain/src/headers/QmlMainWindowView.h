#pragma once
#include "AbstractMainWindowView.h"
#include "AbstractQmlView.h"
#include "QQmlApplicationEngine"
class QmlMainWindowView:public AbstractMainWindowView
{
public:
    QmlMainWindowView();

private:
    QList<std::shared_ptr<AbstractQmlView>> m_viewList;
    QQmlApplicationEngine m_engine;

    // AbstractMainWindowView interface
public:
    void show() override;
    std::shared_ptr<AbstractPresenter> bindView(AbstractPluginManager &) override;



    // AbstractMainWindowView interface
public:
    void appendTextToBrowser(const QString &) override;
};
