#pragma once

#include <QObject>
#include "AbstractMainWindowView.h"
#include "LabPluginLoader.h"
#include "PluginStorageModel.h"

class MainWindowPresenter : public QObject
{
    Q_OBJECT
public:
    explicit MainWindowPresenter(AbstractMainWindowView*,QObject *parent = nullptr);

signals:

public slots:
    void show();

private:
    std::unique_ptr<AbstractMainWindowView> m_view;
    LabPluginLoader m_loader;
    PluginStorageModel m_store;
private slots:
    void appendTextToTextBrowser(const QString&);
};
