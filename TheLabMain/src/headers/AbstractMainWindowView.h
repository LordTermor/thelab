#pragma once
#include "AbstractPluginManager.h"
#include "AbstractPresenter.h"
#include <QtCore>
class AbstractMainWindowView
{
public:
    virtual ~AbstractMainWindowView() = default;
    virtual void show() = 0;
    virtual std::shared_ptr<AbstractPresenter> bindView(AbstractPluginManager&) = 0;
    virtual void appendTextToBrowser(const QString&) = 0;
};

Q_DECLARE_INTERFACE(AbstractMainWindowView,"AbstractMainWindowView")
