#include "MainWindowPresenter.h"
#include <QApplication>
#include "MainWindowView.h"
#include "QmlMainWindowView.h"
#include <QQuickStyle>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
#ifdef QMLVIEW
    QQuickStyle::setStyle("Material");
    MainWindowPresenter w(new QmlMainWindowView);
#endif
#ifdef WIDGETSVIEW
    MainWindowPresenter w(new MainWindowView);
#endif
    w.show();

    return a.exec();
}
