#include "QmlMainWindowView.h"
#include <QQmlContext>
#include <QQuickItem>

QmlMainWindowView::QmlMainWindowView()
{
    m_engine.addImportPath("qrc:/");
    m_engine.load("qrc:/QmlMainWindowView.qml");
}

void QmlMainWindowView::show()
{
    m_engine.rootObjects().first()->setProperty("visible",true);
}

std::shared_ptr<AbstractPresenter> QmlMainWindowView::bindView(AbstractPluginManager &model)
{
    auto view  = std::dynamic_pointer_cast<AbstractQmlViewFactory>(model.getFactory("QML"))->create(&m_engine);
    auto instance = dynamic_cast<QQuickItem*>(view.get());
    m_engine.setObjectOwnership(instance, QQmlEngine::CppOwnership);
    m_viewList.append(view);
    QMetaObject::invokeMethod(m_engine.rootObjects().first(),"append",
                              Q_ARG(QVariant,model.metaInfo().name),
                              Q_ARG(QVariant,QVariant::fromValue(instance)));
    return view->bindPresenter();
}


void QmlMainWindowView::appendTextToBrowser(const QString &)
{
}
