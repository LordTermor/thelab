#include "MainWindowView.h"
#include "ui_MainWindowView.h"
#include <QMessageBox>
#include <QDockWidget>
#include <memory>
MainWindowView::MainWindowView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindowView),
    m_dockWidget(new QMainWindow(this))
{
    ui->setupUi(this);
    ui->verticalLayout->insertWidget(0,m_dockWidget);
}

MainWindowView::~MainWindowView()
{
    delete ui;
}

std::shared_ptr<AbstractPresenter> MainWindowView::bindView(AbstractPluginManager &tab)
{
    auto el = std::dynamic_pointer_cast<AbstractWidgetsViewFactory>(tab.getFactory("Widgets"))->create(this);
    auto metaInfo = tab.metaInfo();
    auto menu = el->getMenu();
    menu->setTitle(tab.metaInfo().name);
    auto aboutPluginAction = new QAction("О плагине",this);

    connect(aboutPluginAction,&QAction::triggered,this,[=](){
        QMessageBox::about(this,QString("О плагине"), QString("%0\n\n%1\n\nВерсия: %2.%3").arg(metaInfo.name)
                           .arg(metaInfo.id).arg(metaInfo.majorVer).arg(metaInfo.minorVer));
    });

    menu->addAction(aboutPluginAction);

    ui->pluginsMenu->addMenu(menu.get());
    QDockWidget *dock = new QDockWidget(metaInfo.name, this);
    auto castedWidget = dynamic_cast<QWidget*>(el.get());
    dock->setWidget(castedWidget);
    castedWidget->resize(castedWidget->width(),800);
    m_dockWidget->addDockWidget(Qt::LeftDockWidgetArea,dock);
    m_viewsMap.insert(metaInfo.id,el);

    return el->bindPresenter();
}

void MainWindowView::showEvent(QShowEvent *event)
{
    QList<QDockWidget *> dockWidgets = m_dockWidget->findChildren<QDockWidget *>();
    if(dockWidgets.length()>0){
    dockWidgets[0]->resize(this->width(),this->height());

    for(int i = 1;i<dockWidgets.length();i++){
        dockWidgets[i]->resize(this->width(),this->height());
        m_dockWidget->tabifyDockWidget(dockWidgets[i-1],dockWidgets[i]);
    }
    }

}

void MainWindowView::resizeEvent(QResizeEvent *event)
{
    QList<QDockWidget *> dockWidgets = m_dockWidget->findChildren<QDockWidget *>();
    for(auto& widget:dockWidgets){
        widget->resize(m_dockWidget->width(),m_dockWidget->height());
    }
}


void MainWindowView::appendTextToBrowser(const QString &text)
{
    ui->logTextBrowser->append(text);
    ui->logTextBrowser->update();
}
