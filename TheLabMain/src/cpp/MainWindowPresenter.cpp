#include "MainWindowPresenter.h"
#include "LoggingBackendQtDebug.h"
#include "LoggingBackendRetranslator.h"
#include "LoggingBackendMonitor.h"
#include "Logger.h"
#include "Utility.h"
#include "Settings.h"
#include "SettingsBackendQSettings.h"
MainWindowPresenter::MainWindowPresenter(AbstractMainWindowView *view, QObject *parent) : QObject(parent)
  ,m_view(std::unique_ptr<AbstractMainWindowView>(view))
  ,m_loader(qApp->applicationDirPath()+"/Plugins")

{

    auto monitor = std::make_shared<LoggingBackendMonitor>();
    monitor->addBackend(std::make_shared<LoggingBackendQtDebug>());
    auto retranslator = std::make_shared<LoggingBackendRetranslator>(&Utility::Singleton<Logger>::instance());
    connect(retranslator.get(),&LoggingBackendRetranslator::infoWritten,this,&MainWindowPresenter::appendTextToTextBrowser);
    connect(retranslator.get(),&LoggingBackendRetranslator::warningWritten,this,&MainWindowPresenter::appendTextToTextBrowser);
    connect(retranslator.get(),&LoggingBackendRetranslator::criticalWritten,this,&MainWindowPresenter::appendTextToTextBrowser);

    monitor->addBackend(retranslator);


    Utility::Singleton<Logger>::instance().setLogger(monitor);

    Utility::Singleton<Settings>::instance().setBackend(std::make_shared<SettingsBackendQSettings>(qApp->applicationDirPath()+"/Settings",QSettings::IniFormat));
    m_loader.load();

    for(auto& el:m_loader.pluginsList()){
        auto metaInfo = el->metaInfo();
        m_store.appendPresenter(metaInfo.id,m_view->bindView(*el));
    }
}

void MainWindowPresenter::show()
{
    m_view->show();
}

void MainWindowPresenter::appendTextToTextBrowser(const QString &st)
{
    m_view->appendTextToBrowser(st);
}
