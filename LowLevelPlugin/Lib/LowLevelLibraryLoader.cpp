#include "LowLevelLibraryLoader.h"
#include "Utility.h"
#include "LoggerProvider.h"
using namespace Utility;
int LowLevelLibraryLoader::doAction(int value)
{
    QObject* obj = m_pluginLoader.instance();

    int res = 0;
    lInfo("Calculating bitwise NOT...");
    QMetaObject::invokeMethod(obj,"binaryNot",
                              Qt::DirectConnection,
                              Q_RETURN_ARG(int,res),
                              Q_ARG(int,value));
    lInfo(QString("Calculated. Result is %1").arg(res));
    return res;
}

bool LowLevelLibraryLoader::load(const QString &string)
{
    m_pluginLoader.setFileName(string);
    if(!m_pluginLoader.load()){
        lCritical(QString("Library %1 not found!").arg(string));
        return false;
    }
    return true;
}
