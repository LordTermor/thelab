#pragma once
#include <QPluginLoader>

class LowLevelLibraryLoader
{
public:
    LowLevelLibraryLoader()=default;
    int doAction(int value);
    bool load(const QString&);
private:
    QPluginLoader m_pluginLoader;
};
