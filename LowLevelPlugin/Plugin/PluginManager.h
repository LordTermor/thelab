#pragma once
#include "AbstractPluginManager.h"
#include "AbstractQmlViewFactory.h"
#include "AbstractWidgetsViewFactory.h"
#include "LowLevelLibraryPresenter.h"
class PluginManager : public AbstractPluginManager
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID PLUGINID FILE "plugin.json")
    Q_INTERFACES(AbstractPluginManager)
public:
    PluginManager();

    // AbstractPluginModel interface
public:


private:
    std::shared_ptr<AbstractQmlViewFactory> m_qmlFactory;
    std::shared_ptr<AbstractWidgetsViewFactory> m_widgetsFactory;


    // AbstractPluginManager interface
public:
    std::shared_ptr<AbstractViewFactory> getFactory(const QString &viewType) const override;
    QStringList getViewTypes() const override;
};
