#pragma once

#include <QQuickItem>
#include <memory>
#include "AbstractPluginView.h"
#include "AbstractQmlView.h"
#include "LowLevelLibraryPresenter.h"

class AndroidView:public QQuickItem,public AbstractPluginView,
        public AbstractQmlView,public std::enable_shared_from_this<AndroidView>
{
    Q_OBJECT
    Q_INTERFACES(AbstractPluginView)
    Q_INTERFACES(AbstractQmlView)
public:
    AndroidView(QQuickItem* child,QQuickItem* parent = nullptr):QQuickItem(parent),AbstractPluginView(),AbstractQmlView (),child(child){
        child->setParent(this);
        child->setParentItem(this);
        connect(child->findChild<QQuickItem*>("requestOperationButton"),SIGNAL(clicked()),
                this,SIGNAL(onOperationRequested()),Qt::DirectConnection);
    }

    virtual ~AndroidView() override = default;


    // AbstractQmlView interface
    void setOuput(int) override;
    int getInput() override;
signals:
    void onOperationRequested() override;


    // AbstractView interface
public:
    std::shared_ptr<AbstractPresenter> bindPresenter() override{
        return std::make_shared<LowLevelLibraryPresenter>(shared_from_this());
    }
private:
    QQuickItem* child;

    // AbstractPluginView interface
public:
    void setError(bool) override;

    // AbstractPluginView interface
signals:
    void onReloadLibRequested(const QString &) override;

    // AbstractView interface
public:
    std::shared_ptr<QMenu> getMenu() override;
};
