#include "WidgetsView.h"
#include "ui_View.h"
#include <QFileDialog>
WidgetsView::WidgetsView(QWidget *parent):
    QWidget (parent),
    AbstractWidgetsView(),
    AbstractPluginView(),
    ui(new Ui::View)
{
    ui->setupUi(this);

    auto loadLambda = [=](){
        QString path = QFileDialog::getOpenFileName(this,"Выберите библиотеку");
        emit this->onReloadLibRequested(path);
    };
    connect(ui->requestButton,&QPushButton::clicked,this,&WidgetsView::onOperationRequested);
    connect(ui->pushButton,&QPushButton::clicked,this,loadLambda);

    m_menu = std::make_shared<QMenu>(this);

    auto action = new QAction("Загрузить библиотеку");
    connect(action,&QAction::triggered,this,loadLambda);

    m_menu->addActions({action});
}

void WidgetsView::setOuput(int val)
{
    ui->inputSpinBox->setValue(val);
}

int WidgetsView::getInput()
{
    return ui->inputSpinBox->value();
}

void WidgetsView::setError(bool error)
{
    ui->warningFrame->setVisible(error);

    ui->inputSpinBox->setEnabled(!error);
    ui->requestButton->setEnabled(!error);
}

std::shared_ptr<QMenu> WidgetsView::getMenu(){
    return m_menu;
}
