#pragma once
#include <QWidget>
#include "AbstractPluginView.h"
#include "AbstractWidgetsView.h"
#include "LowLevelLibraryPresenter.h"

namespace Ui{
    class View;
}

class WidgetsView:public QWidget,public AbstractWidgetsView,
        public AbstractPluginView,public std::enable_shared_from_this<WidgetsView>
{
    Q_OBJECT
    Q_INTERFACES(AbstractPluginView)
    Q_INTERFACES(AbstractWidgetsView)

public:
    WidgetsView(QWidget* parent = nullptr);
    virtual ~WidgetsView() override = default;

    // AbstractView interface

    void setOuput(int) override;
    int getInput() override;
    void setError(bool) override;
protected:
    Ui::View* ui;
    std::shared_ptr<QMenu> m_menu;

signals:
    void onOperationRequested() override;

    // AbstractView interface
public:
    std::shared_ptr<AbstractPresenter> bindPresenter() override{
        return std::make_shared<LowLevelLibraryPresenter>(shared_from_this());
    }


    // AbstractPluginView interface
signals:
    void onReloadLibRequested(const QString &) override;

    // AbstractView interface
public:
    std::shared_ptr<QMenu> getMenu() override;
};
