#pragma once
#include <QtCore>
class AbstractPluginView
{
public:
    virtual ~AbstractPluginView()=default;
    virtual void setOuput(int) = 0;
    virtual int getInput() = 0;
    virtual void setError(bool) = 0;

signals:
    virtual void onOperationRequested() = 0;
    virtual void onReloadLibRequested(const QString&) = 0;

};
Q_DECLARE_INTERFACE(AbstractPluginView,"AbstractPluginView")
