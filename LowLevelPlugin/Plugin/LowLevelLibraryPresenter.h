#pragma once
#include <QObject>
#include "AbstractPluginView.h"
#include "LowLevelLibraryLoader.h"
#include "AbstractPresenter.h"
#include <memory>
class LowLevelLibraryPresenter:public QObject,public AbstractPresenter
{
    Q_OBJECT
    Q_INTERFACES(AbstractPresenter)
public:
    LowLevelLibraryPresenter(std::shared_ptr<AbstractPluginView> view,QObject* parent = nullptr):
        QObject(parent),
        m_view(view),
        m_loader(std::make_shared<LowLevelLibraryLoader>())
    {
        connect(dynamic_cast<QObject*>(m_view.get()),SIGNAL(onOperationRequested()),this,SLOT(onNotButtonClicked()));
        connect(dynamic_cast<QObject*>(m_view.get()),SIGNAL(onReloadLibRequested(const QString &)),
                this,SLOT(onLoadLibClicked(const QString &)));
    }
    ~LowLevelLibraryPresenter() = default;
protected:
    std::shared_ptr<AbstractPluginView> m_view;
    std::shared_ptr<LowLevelLibraryLoader> m_loader;
protected slots:
    void onNotButtonClicked();
    void onLoadLibClicked(const QString &path);
};
