#pragma once
#include "AbstractQmlViewFactory.h"
#include "AndroidView.h"

class AndroidViewFactory:public AbstractQmlViewFactory
{
public:
    virtual ~AndroidViewFactory() override = default;
    // ViewFactory interface
public:
    std::shared_ptr<AbstractQmlView> create(QQmlEngine* engine = nullptr) override{
        QQmlComponent component(engine,"qrc:/"+QString(PLUGINID)+"/AndroidView.qml");
        QQuickItem* child = qobject_cast<QQuickItem*>(component.create());
        return std::make_shared<AndroidView>(child);
    }
};
