import QtQuick 2.0
import QtQuick.Controls 2.12
Page {
    anchors.fill: parent
    Column{
        spacing: 10
        anchors.top:parent.top
        width: parent.width
        SpinBox{
            from:-32000
            to:32000
            anchors.horizontalCenter: parent.horizontalCenter
            objectName: "inputSpinBox"
            id:input
            width: parent.width
        }
        Button{
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            objectName: "requestOperationButton"
            id:operationButton
            text: "Битовое НЕ"
        }
    }
}
