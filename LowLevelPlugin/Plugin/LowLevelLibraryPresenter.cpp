#include "LowLevelLibraryPresenter.h"

void LowLevelLibraryPresenter::onNotButtonClicked()
{
    m_view->setOuput(m_loader->doAction(m_view->getInput()));
}

void LowLevelLibraryPresenter::onLoadLibClicked(const QString& path)
{
    bool success = m_loader->load(path);
    m_view->setError(!success);
}
